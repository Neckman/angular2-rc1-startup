# Angular 2 RC 1 Startup 

This repo is here to help startup an angular 2 RC 1 application.  

## Installation

```
$ git clone https://gitlab.com/nashvegastech/angular2-rc1-startup.git
$ cd angular2-rc1-startup
$ npm install
```
This project makes use of a couple of packages that need to be installed globally.

Pug
```
$ npm install -g pug
$ npm install -g pug-cli
```
Typescript
```
$ npm install -g typings
$ npm install -g typescript
```
Sass
```
$ npm install -g node-sass
```